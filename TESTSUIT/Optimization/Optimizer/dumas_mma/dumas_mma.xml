<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation">
  <documentation>
    <title>Jérémie Dumas MMA Implementation</title>
    <authors>
      <author>Fabia Wein</author>
    </authors>
    <date>2024-09-30</date>
    <keywords>
      <keyword>SIMP</keyword>
    </keywords>
    <description>Uses the C++ MMA implementation by Jérémie Dumas, based on Niels Aaage's PETSc MMA, based on Svanberg's MMA.
    dumas_mma is MMA, dumas_gcmma is the variant with line search and monotonous decrease. 
    The problem solved is 
           Minimize  f_0(x) + a_0*z + sum( c_i*y_i + 0.5*d_i*(y_i)^2 )
         subject to  f_i(x) - a_i*z - y_i bounded by 0,  i = 1,...,m
                     xmin_j lower x_j bounded by xmax_j,    j = 1,...,n
                     z and y_i not negative,         i = 1,...,m */
    the move_limit can be tuned as the native mma and ocm.
    </description>
  </documentation>

  <fileFormats>
    <output>
      <hdf5 />
      <info />
    </output>
    <materialData file="mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region material="99lines" name="mech" />
    </regionList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <mechanic subType="planeStrain">
        <regionList>
          <region name="mech" />
        </regionList>

        <bcsAndLoads>
           <fix name="west"> 
              <comp dof="x"/> <comp dof="y"/> 
           </fix>
           <force name="right_lower">
            <comp dof="y" value="-1"/>
          </force>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions />
          </nodeResult>
          <elemResult type="mechPseudoDensity">
            <allRegions/>
          </elemResult>
          <elemResult type="physicalPseudoDensity">
            <allRegions/>
          </elemResult>
        </storeResults>
      </mechanic>
    </pdeList>
    <linearSystems>
      <system>
        <solverList>
          <directLDL id="default"/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
  
  
  <optimization>
    <costFunction type="compliance" task="minimize" > 
      <stopping queue="2" value="0.02" type="designChange"/>
    </costFunction>
    
    <constraint type="volume" bound="upperBound" value="0.5" />
    
    <optimizer type="dumas_mma" maxIterations="300">
      <scale target="10"/>
    </optimizer>
      
    <ersatzMaterial region="mech" material="mechanic" method="simp">
      <filters>
        <filter neighborhood="maxEdge" value="1.5" type="density"/>
      </filters>
      
     <design name="density" initial="0.5" lower="1e-3" upper="1.0" />
      
     <transferFunction type="simp" application="mech" param="3" /> 
     <export write="iteration" save="all"/>
      
    </ersatzMaterial>
    <commit mode="forward" stride="1"/>
  </optimization>
  
</cfsSimulation>


