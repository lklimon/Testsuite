<?xml version="1.0"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
  <documentation>
    <title>3D (Pseudo 1D) AcousticMixed Channel Calculation with ABC and Spectral FEM</title>
    <authors>
      <author>ahueppe</author>
    </authors>
    <date>2012-01-13</date>
    <keywords>
      <keyword>acoustic</keyword>
      <keyword>transient</keyword>
      <keyword>spectral fem</keyword>
      <keyword>mixed fem</keyword>
    </keywords>
    <references>
      Any Acoustic Book
    </references>
    <isVerified>yes</isVerified>
    <description>
      This trivial acoustic examle consists of a three-dimensional rectangular tube
      filled with air. A constant sine exitation is applied to the source 
      surface element and a first order abc is applied on the opposite element
      As we use only one element for discretization of the tube diameter this example 
      can be seen as pseudo 1D and the acoustic wave should travel without amplitude decay
      to the record node. Also, as this is a plane wave we expect no reflections from
      the abc boundary.
      
      We test the acousticMixed PDE which has two FeFunctions one for particle velocity and 
      one for acoustic pressure.
      
      We use a very coarse discretization (approx 2 elements per wavelength) 
      in combination with 3rd order spectral elements.
      Additionally some elements in the mesh are rotated to test for the point
      permutations used when computing the FeSpaceH1Nodal equation numbers. 
    </description>
  </documentation>


  <fileFormats>
    <input>
      <hdf5 fileName="channel3d_sfem_abc.h5"/>
    </input>
    <output>
      <text id="1"/>
      <hdf5 id="2"/>
    </output>
    <materialData file="mat.xml" format="xml"/>    
  </fileFormats>
  
  <domain geometryType="3d">
    <regionList>
      <region name="water" material="air_approx"/>
    </regionList>
    <surfRegionList>
      <surfRegion name="source"/>
      <surfRegion name="abc"/>
    </surfRegionList>

    <nodeList>
      <nodes name="savNodes">
        <coord x="0" y="0" z="5.4e-3"/>
      </nodes>
      
    </nodeList>
  </domain>
  <fePolynomialList>
  <Lagrange id="default" spectral="true">
    <isoOrder serendipity="false">3</isoOrder>
  </Lagrange>
  </fePolynomialList>

  <sequenceStep>
    <analysis>
      <transient>
        <numSteps>300</numSteps>
        <deltaT>2.00000000e-7</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <acousticMixed>
        <regionList>
          <region name="water"/>
        </regionList>
        
        <bcsAndLoads>
          <pressure name="source" value="1.0*sin(8e4*2*pi*t)"/>
          <absorbingBCs name="abc" volumeRegion="water"/>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="acouPressure">
            <allRegions saveInc="5" saveBegin="1"/>
            <nodeList>
              <nodes name="savNodes"/>
            </nodeList>
          </nodeResult>
        </storeResults>
      </acousticMixed>
    </pdeList>
    <linearSystems>
      <system>
        <solverList>
          <directLU/>
        </solverList>
      </system>
    </linearSystems>
    
  </sequenceStep>
  
</cfsSimulation>
