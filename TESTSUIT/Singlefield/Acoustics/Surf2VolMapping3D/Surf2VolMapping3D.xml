<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
               xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd"
               xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>Surf2VolMapping3D</title>
    <authors>
      <author>pheidegger</author>
    </authors>
    <date>2023-09-11</date>
    <keywords>
      <keyword>acoustic</keyword>
      <keyword>harmonic</keyword>
    </keywords>
    <references> 
      Non Just functionality test
    </references>
    <isVerified>yes</isVerified>
    <description>
      This test aims to cover the correct funciton of the integration point mapping between volume and surface elements.
      It contains Hex, Wedge, Tet, and Pyramid elements and applies a normalVelocity BC on all surfaces to 
      cover all possible cases. The correct mapping is tested indirectly, in an assert for the debug build.
      In the hope to being able to test also in a non-debug build, a gradient field is applied on the BCs.
    </description>
  </documentation>
  <!--***************************    INPUTs and OUTPUTs    ***********************-->
  <fileFormats> 
    <input>
      <hdf5 fileName="./Surf2VolMapping3D.h5ref"/>
    </input>
    <output>
      <hdf5 id="hdf5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  <!--***************************  REGIONSs, SURFACEs and NODEs  ***********************-->
  <domain geometryType="3d">
    <variableList>
      <var name="c0" value="1"/>
      <var name="p0" value="1"/>
      <var name="rho" value="1"/>
      <var name="f0" value="1"/>
    </variableList>
    <regionList> 
      <region name="V_Hex" material="fakefluid"/>
      <region name="V_Wedge" material="fakefluid"/>
      <region name="V_Pyram" material="fakefluid"/>
      <region name="V_Tet" material="fakefluid"/>
    </regionList>
    <surfRegionList>
      <surfRegion name="S_Hex1"/>
      <surfRegion name="S_Hex2"/>
      <surfRegion name="S_Hex3"/>
      <surfRegion name="S_Hex4"/>
      <surfRegion name="S_Hex5"/>
      <surfRegion name="S_Hex6"/>
      <surfRegion name="S_Wedge1"/>
      <surfRegion name="S_Wedge2"/>
      <surfRegion name="S_Wedge3"/>
      <surfRegion name="S_Wedge4"/>
      <surfRegion name="S_Wedge5"/>
      <surfRegion name="S_Pyram1"/>
      <surfRegion name="S_Pyram2"/>
      <surfRegion name="S_Pyram3"/>
      <surfRegion name="S_Pyram4"/>
      <surfRegion name="S_Pyram5"/>
      <surfRegion name="S_Tet1"/>
      <surfRegion name="S_Tet2"/>
      <surfRegion name="S_Tet3"/>
      <surfRegion name="S_Tet4"/>
      <surfRegion name="S_HexEx1"/>
      <surfRegion name="S_HexEx2"/>
      <surfRegion name="S_HexEx3"/>
      <surfRegion name="S_HexEx4"/>
      <surfRegion name="S_HexEx5"/>
      <surfRegion name="S_HexEx6"/>
      <surfRegion name="S_HexEx7"/>
      <surfRegion name="S_TetEx1"/>
      <surfRegion name="S_TetEx2"/>
      <surfRegion name="S_TetEx3"/>
      <surfRegion name="S_TetEx4"/>
      <surfRegion name="S_TetEx5"/>
      <surfRegion name="S_TetEx6"/>
      <surfRegion name="S_TetEx7"/>
      <surfRegion name="S_TetEx8"/>
      <surfRegion name="S_TetEx9"/>
      <surfRegion name="S_TetEx10"/>
    </surfRegionList>
  </domain>
  <!--***************************  ANALYSIS  ***********************-->
  <sequenceStep index="1">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="V_Hex"/>
          <region name="V_Pyram"/>
          <region name="V_Tet"/>
          <region name="V_Wedge"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="S_Hex1" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Hex2" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Wedge1" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Wedge2" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Pyram1" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Pyram2" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Tet1" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Tet2" value="p0*(x+y+z)"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="V_Hex" />
              <region name="V_Pyram" />
              <region name="V_Tet" />
              <region name="V_Wedge" />
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>

  <sequenceStep index="2">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="V_Hex"/>
          <region name="V_Pyram"/>
          <region name="V_Tet"/>
          <region name="V_Wedge"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="S_Hex4" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Hex5" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Hex6" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Wedge4" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Wedge5" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Pyram4" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Pyram5" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Tet3" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Tet4" value="p0*(x+y+z)"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="V_Hex"/>
              <region name="V_Pyram"/>
              <region name="V_Tet"/>
              <region name="V_Wedge"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>

   <sequenceStep index="3">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="V_Hex"/>
          <region name="V_Pyram"/>
          <region name="V_Tet"/>
          <region name="V_Wedge"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="S_HexEx7" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Hex3" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Wedge3" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Pyram3" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Tet1" value="p0*(x+y+z)"/>
          <normalVelocity name="S_Tet2" value="p0*(x+y+z)"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="V_Hex"/>
              <region name="V_Pyram"/>
              <region name="V_Tet"/>
              <region name="V_Wedge"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>

  <sequenceStep index="4">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="V_Hex"/>
          <region name="V_Tet"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="S_HexEx1" value="p0*(x+y+z)"/>
          <normalVelocity name="S_TetEx1" value="p0*(x+y+z)"/>
          <normalVelocity name="S_TetEx10" value="p0*(x+y+z)"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="V_Hex"/>
              <region name="V_Tet"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>

   <sequenceStep index="5">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="V_Hex"/>
          <region name="V_Tet"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="S_HexEx2" value="p0*(x+y+z)"/>
          <normalVelocity name="S_TetEx2" value="p0*(x+y+z)"/>
          <normalVelocity name="S_TetEx9" value="p0*(x+y+z)"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="V_Hex"/>
              <region name="V_Tet"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>

  <sequenceStep index="6">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="V_Hex"/>
          <region name="V_Tet"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="S_HexEx3" value="p0*(x+y+z)"/>
          <normalVelocity name="S_TetEx3" value="p0*(x+y+z)"/>
          <normalVelocity name="S_TetEx8" value="p0*(x+y+z)"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="V_Hex"/>
              <region name="V_Tet"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>

   <sequenceStep index="7">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="V_Hex"/>
          <region name="V_Tet"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="S_HexEx4" value="p0*(x+y+z)"/>
          <normalVelocity name="S_TetEx4" value="p0*(x+y+z)"/>
          <normalVelocity name="S_TetEx7" value="p0*(x+y+z)"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="V_Hex"/>
              <region name="V_Tet"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>

  <sequenceStep index="8">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="V_Hex"/>
          <region name="V_Tet"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="S_HexEx5" value="p0*(x+y+z)"/>
          <normalVelocity name="S_TetEx5" value="p0*(x+y+z)"/>
          <normalVelocity name="S_TetEx6" value="p0*(x+y+z)"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="V_Hex"/>
              <region name="V_Tet"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>

   <sequenceStep index="9">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="V_Hex"/>
          <region name="V_Tet"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="S_HexEx6" value="p0*(x+y+z)"/>
          <normalVelocity name="S_TetEx6" value="p0*(x+y+z)"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="V_Hex"/>
              <region name="V_Tet"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>
</cfsSimulation>
