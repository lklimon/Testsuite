<cfsSimulation xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
https://opencfs.gitlab.io/cfs/xml/CFS-Simulation/CFS.xsd">

  <documentation>
    <title>PostProcBackCouplingChamber2D</title>
    <authors>
      <author>Dominik Mayrhofer</author>
    </authors>
    <date>2024-06-20</date>
    <keywords>
      <keyword>acoustic</keyword>
      <keyword>electrostatic</keyword>
      <keyword>mechanic</keyword>
      <keyword>smooth</keyword>
      <keyword>output</keyword>
    </keywords>
    <references> 
          Non Just functionality test
    </references>
    <isVerified>yes</isVerified>
    <description>
   	  Multiple tests to check the arbitrary back-coupling via postProc results.
   	  By using the "function" function of "postProcList" we can arbitrarily post-process 
      a primary results or a post-processing result, which can then be back-coupled by 
      enabling the creation of a coefFunction and using a free result-type 
      (GENERIC_RESULT_1 to GENERIC_RESULT_5). Multiple results can be used at once, but 
      the result-type has to be unique. We can also create scalar functions from vectorial 
      one and vice-versa, or just use the initial dimension of the quanitity. The only real 
      limitation is given by the functions usable in the math-parser.
    </description>
  </documentation>

  <fileFormats>
    <input>
      <!--<cdb fileName="PostProcBackCouplingChamber2D.cdb"/>-->
      <hdf5 fileName="PostProcBackCouplingChamber2D.h5ref"/>
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <variableList>
      <var name="b" value="1e-3"/>
      <var name="h" value="1e-3"/>
      <var name="f_exc" value="50"/>
    </variableList>
    
    <regionList>
      <region name="Air_l" material="FluidMat"/>
      <region name="Air_r" material="FluidMat"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="BC_Air_t_l"/>
      <surfRegion name="BC_Air_l"/>
      <surfRegion name="BC_Air_b_l"/>
      <surfRegion name="BC_Air_mid"/>
      <surfRegion name="BC_Air_t_r"/>
      <surfRegion name="BC_Air_b_r"/>
      <surfRegion name="BC_Air_r"/>
    </surfRegionList>
  </domain>
  
  <sequenceStep index="1"> <!-- scalar to scalar test -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>10</numSteps>
        <deltaT>1e-3</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="Air_l"/>
        </regionList>

        <bcsAndLoads>
          <pressure name="BC_Air_l" value="sin(2*pi*f_exc*t)"/>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="acouPressure">
            <allRegions outputIds="h5" postProcId="acou"/>
          </nodeResult>
        </storeResults>
      </acoustic>

      <electrostatic>
        <regionList>
          <region name="Air_r"/>
        </regionList>

        <bcsAndLoads>
          <ground name="BC_Air_r"/>
          <potential name="BC_Air_mid">
            <coupling pdeName="acoustic">
              <quantity name="genericResult_1" />
            </coupling>
          </potential>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="elecPotential">
            <allRegions outputIds="h5"/>
          </nodeResult>
        </storeResults>
      </electrostatic>
    </pdeList>
    
    <couplingList>
      <iterative PDEorder="acoustic;electrostatic">
        <convergence logging="yes" maxNumIters="20" stopOnDivergence="no">
          <!-- We always define a coupling quantity since only those will be checked -->
          <quantity name="genericResult_1" value="1e-3" normType="rel" avoidIntegratedNorm="yes"/> 
          <!-- Here we set the flag to avoid integration since the weight for the numerical integration is 0. 
          This will result in a zero-valued norm either way, which can and most likely will lead to problems
          (ranging from lagging-results to generally wrong iterations).-->
          <!-- For the name we could potentially also used a different one, but ideally the reserved names
           genericResult_0 to genericResult_9 should be used. Only those can be used to verify the XML scheme
           (although others should work too, they are not tested). -->
        </convergence>
      </iterative>
    </couplingList>

    <postProcList>
      <postProc id="acou">
        <function resultName="genericResult_1">
          <inputResult variableName="u"/>
          <scalar realFunc="2*u"/>
        </function>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="2"> <!-- scalar to vector test -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>10</numSteps>
        <deltaT>1e-3</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="Air_l"/>
        </regionList>

        <bcsAndLoads>
          <pressure name="BC_Air_l" value="sin(2*pi*f_exc*t)"/>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="acouPressure">
            <allRegions outputIds="h5" postProcId="acou"/>
          </nodeResult>
        </storeResults>
      </acoustic>
      
      <smooth subType="planeStrain">
        <regionList>
          <region name="Air_l"/>
        </regionList>
        <bcsAndLoads>
          <fix name="BC_Air_t_l">
            <comp dof="y"/>
          </fix>
          <fix name="BC_Air_b_l">
            <comp dof="y"/>
          </fix>
          <fix name="BC_Air_l">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <displacement name="BC_Air_mid">
            <coupling pdeName="acoustic">
              <quantity name="genericResult_2"/>
            </coupling>
          </displacement>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5"/>
          </nodeResult>
        </storeResults>
      </smooth>
    </pdeList>
    
    <couplingList>
      <iterative PDEorder="acoustic;smooth">
        <convergence logging="yes" maxNumIters="20" stopOnDivergence="no">
          <quantity name="genericResult_2" value="1e-3" normType="rel" avoidIntegratedNorm="yes"/>
        </convergence>
      </iterative>
    </couplingList>

    <postProcList>
      <postProc id="acou">
        <function resultName="genericResult_2">
          <inputResult variableName="u"/>
          <vector>
            <comp dof="x" realFunc="u/1e4"/>
            <comp dof="y" realFunc="0"/>
          </vector>
        </function>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="3"> <!-- vector to scalar test -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>10</numSteps>
        <deltaT>1e-3</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Air_l"/>
        </regionList>
        <bcsAndLoads>
          <fix name="BC_Air_t_l">
            <comp dof="y"/>
          </fix>
          <fix name="BC_Air_b_l">
            <comp dof="y"/>
          </fix>
          <fix name="BC_Air_l">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <displacement name="BC_Air_mid">
            <comp dof="x" value="b/10*sin(2*pi*f_exc*t)"/>
            <comp dof="y" value="0"/>
          </displacement>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5" postProcId="disp"/>
          </nodeResult>
        </storeResults>
      </smooth>

      <acoustic formulation="acouPressure">
        <regionList>
          <region name="Air_l"/>
        </regionList>

        <bcsAndLoads>
          <pressure name="BC_Air_mid">
            <coupling pdeName="smooth">
              <quantity name="genericResult_3"/>
            </coupling>
          </pressure>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="acouPressure">
            <allRegions outputIds="h5"/>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
    
    <couplingList>
      <iterative PDEorder="smooth;acoustic">
        <convergence logging="yes" maxNumIters="20" stopOnDivergence="no">
          <quantity name="genericResult_3" value="1e-3" normType="rel" avoidIntegratedNorm="yes"/>
        </convergence>
      </iterative>
    </couplingList>

    <postProcList>
      <postProc id="disp">
        <function resultName="genericResult_3">
          <inputResult variableName="u"/>
          <scalar realFunc="1e5*(ux+uy)"/>
        </function>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="4"> <!-- vector to vector test -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>10</numSteps>
        <deltaT>5e-4</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Air_l"/>
        </regionList>
        <bcsAndLoads>
          <fix name="BC_Air_t_l">
            <comp dof="y"/>
          </fix>
          <fix name="BC_Air_b_l">
            <comp dof="y"/>
          </fix>
          <fix name="BC_Air_l">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <displacement name="BC_Air_mid">
            <comp dof="x" value="b/10*sin(2*pi*f_exc*t)"/>
            <comp dof="y" value="0"/>
          </displacement>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5" postProcId="disp"/>
          </nodeResult>
        </storeResults>
      </smooth>

      <mechanic subType="planeStrain">
        <regionList>
          <region name="Air_r"/>
        </regionList>

        <bcsAndLoads>
          <fix name="BC_Air_t_r">
            <comp dof="y"/>
          </fix>
          <fix name="BC_Air_b_r">
            <comp dof="y"/>
          </fix>
          <fix name="BC_Air_r">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <displacement name="BC_Air_mid">
            <coupling pdeName="smooth">
              <quantity name="genericResult_4"/>
            </coupling>
          </displacement>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions outputIds="h5"/>
          </nodeResult>
        </storeResults>
      </mechanic>
    </pdeList>
    
    <couplingList>
      <iterative PDEorder="smooth;mechanic">
        <convergence logging="yes" maxNumIters="20" stopOnDivergence="no">
          <quantity name="genericResult_4" value="1e-3" normType="rel" avoidIntegratedNorm="yes"/>
        </convergence>
      </iterative>
    </couplingList>

    <postProcList>
      <postProc id="disp">
        <function resultName="genericResult_4">
          <inputResult variableName="u"/>
          <vector>
            <comp dof="x" realFunc="min(ux,b/20)"/>
            <comp dof="y" realFunc="0"/>
          </vector>
        </function>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="5"> <!-- everything coupled to everything with multiple results and mixing nodal/surface evaluation -->
    <analysis>
      <transient initialTime="zero">
        <numSteps>10</numSteps>
        <deltaT>5e-4</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="Air_l"/>
        </regionList>

        <bcsAndLoads>
          <pressure name="BC_Air_l" value="sin(2*pi*f_exc*t)"/>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="acouPressure">
            <allRegions outputIds="h5" postProcId="acou"/> <!-- scalar-scalar -->
          </nodeResult>
        </storeResults>
      </acoustic>

      <electrostatic>
        <regionList>
          <region name="Air_r"/>
        </regionList>

        <bcsAndLoads>
          <ground name="BC_Air_r"/>
          <potential name="BC_Air_mid">
            <coupling pdeName="acoustic">
              <quantity name="genericResult_1"/> <!-- scalar-scalar -->
            </coupling>
          </potential>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="elecPotential">
            <allRegions outputIds="h5"/>
          </nodeResult>
          <surfElemResult type="elecForceDensity">
            <surfRegionList>
              <surfRegion name="BC_Air_mid" postProcId="force"/> <!-- vector-vector (surface) -->
            </surfRegionList>
          </surfElemResult>
        </storeResults>
      </electrostatic>

      <mechanic subType="planeStrain">
        <regionList>
          <region name="Air_r"/>
        </regionList>

        <bcsAndLoads>
          <fix name="BC_Air_t_r">
            <comp dof="y"/>
          </fix>
          <fix name="BC_Air_b_r">
            <comp dof="y"/>
          </fix>
          <fix name="BC_Air_r">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <forceDensity name="BC_Air_mid">
            <coupling pdeName="electrostatic">
              <quantity name="genericResult_2"/> <!-- vector-vector (surface) -->
            </coupling>
          </forceDensity>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions outputIds="h5" postProcId="disp"/> <!-- vector-vector -->
          </nodeResult>
        </storeResults>
      </mechanic>

      <smooth subType="planeStrain">
        <regionList>
          <region name="Air_l"/>
        </regionList>
        <bcsAndLoads>
          <fix name="BC_Air_t_l">
            <comp dof="y"/>
          </fix>
          <fix name="BC_Air_b_l">
            <comp dof="y"/>
          </fix>
          <fix name="BC_Air_l">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <displacement name="BC_Air_mid">
            <coupling pdeName="mechanic">
              <quantity name="genericResult_5"/> <!-- vector-vector (we only use genericResult_5 since we haven't used it up to now) -->
            </coupling>
          </displacement>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions outputIds="h5" postProcId="disp"/>
          </nodeResult>
        </storeResults>
      </smooth>
    </pdeList>
    
    <couplingList>
      <iterative PDEorder="acoustic;electrostatic;mechanic;smooth">
        <convergence logging="yes" maxNumIters="20" stopOnDivergence="no">
          <quantity name="genericResult_1" value="1e-3" normType="rel" avoidIntegratedNorm="yes"/>
          <quantity name="genericResult_2" value="1e-3" normType="rel"/>
          <quantity name="genericResult_5" value="1e-3" normType="rel" avoidIntegratedNorm="yes"/>
        </convergence>
      </iterative>
    </couplingList>

    <postProcList>
      <postProc id="acou">
        <function resultName="genericResult_1">
          <inputResult variableName="u"/>
          <scalar realFunc="2*u"/>
        </function>
      </postProc>

      <postProc id="force">
        <function resultName="genericResult_2">
          <inputResult variableName="u"/>
          <vector>
            <comp dof="x" realFunc="1e12*ux"/>
            <comp dof="y" realFunc="0"/>
          </vector>
        </function>
      </postProc>

      <postProc id="disp">
        <function resultName="genericResult_5">
          <inputResult variableName="v"/> <!-- Just testing if something else than the default "u" also works-->
          <vector>
            <comp dof="x" realFunc="min(vx,1e-7)"/>
            <comp dof="y" realFunc="0"/>
          </vector>
        </function>
      </postProc>
    </postProcList>
  </sequenceStep>
</cfsSimulation>
