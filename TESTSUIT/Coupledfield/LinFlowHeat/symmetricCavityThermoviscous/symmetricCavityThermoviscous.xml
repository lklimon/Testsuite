<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../Devel/CFS_SRC/CFS/share/xml/CFS-Simulation/CFS.xsd"
 xmlns="http://www.cfs++.org/simulation">

 <documentation>
  <title>Channel flow - Linearized Navier-Stokes with basic flow</title>
  <authors>
    <author>Lorenz Klimon</author>
  </authors>
  <date>2022-12-09</date>
  <keywords>
    <keyword>CFD</keyword>
  </keywords>    
  <references>
  </references>
  <isVerified>yes</isVerified>
  <description> 
     Simple long cavity computing viscosthermal acoustics with isothermal BC
     Thermal boundary layers are studied
     Temperature, velocity and pressure field are compared with comsol
     This testcase is the nearly the same, as for the asymmetric formulation created by Hamideh Hassanpour. 
     It was only copied and changed to the symmetric formulation, it also tests against the solution of the
     asymmetric formulation, since this is varified by its testcase.
  </description>
</documentation>

  <fileFormats>
    <input> 
      <hdf5 fileName="symmetricCavityThermoviscous.h5ref"/>
    </input>
    <output>
      <hdf5/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <regionList>
      <region name="S_flow" material="FluidMat"/>
    </regionList>
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="velPolyId">
       <isoOrder>2</isoOrder> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="presPolyId">
       <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>
  
  <sequenceStep>
    <analysis>
      <harmonic>
        <numFreq>4</numFreq>
        <frequencyList>
          <freq value="100"/>
          <freq value="1000"/>
          <freq value="5000"/>
          <freq value="10000"/>
        </frequencyList>
      </harmonic>
    </analysis>
    
    <pdeList>
      <fluidMechLin formulation="compressible" presPolyId="presPolyId" velPolyId="velPolyId">
        <regionList>
          <region name="S_flow"/>
        </regionList>
        
        <presSurfaceList>
          <presSurf name="L_bottom"/>
          <presSurf name="L_top"/>         
          <presSurf name="L_right"/>
        </presSurfaceList>
       
        <bcsAndLoads> 
          <normalTraction name="L_left"  value="1" volumeRegion="S_flow"/>
          <pressure name="L_right" value="0"/>
          <noSlip name="L_top">
            <comp dof="y"/>         
          </noSlip>
          <noSlip name="L_bottom">
            <comp dof="x"/>         
            <comp dof="y"/>
          </noSlip>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions/>         
          </nodeResult>      
        </storeResults>
      </fluidMechLin>
      
      <heatConduction>
        <regionList>
          <region name="S_flow"/>
        </regionList>
        
        <bcsAndLoads>
          <temperature name="L_bottom" value="0" />          
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions/>
          </nodeResult>
        </storeResults>       
      </heatConduction>
    </pdeList>
    
    <couplingList>
      <direct>
        <linFlowHeatDirect symmetric="yes">
          <regionList>
            <region  name="S_flow"/>
          </regionList>
        </linFlowHeatDirect>
      </direct>
    </couplingList>
  </sequenceStep>
</cfsSimulation>
