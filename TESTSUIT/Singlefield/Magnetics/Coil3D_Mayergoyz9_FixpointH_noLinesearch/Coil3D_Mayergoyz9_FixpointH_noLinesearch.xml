<?xml version="1.0"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  
  <documentation>
    <title>3D cylindrical coil with hysteretic core material</title>
    <authors>
      <author>Michael Nierla</author>
    </authors>
    <date>2019-07-09</date>
    <keywords>
      <keyword>magneticEdge</keyword>
      <keyword>hysteresis</keyword>
      <keyword>linesearch</keyword>
      <keyword>transient</keyword>
    </keywords>
    <references>none</references>
    <isVerified>no</isVerified>
    <description>
      Geometry:
	  3D-eigth model of a cylindrical copper coil with iron/FeCo core
	  surrounded by air; same setup as in Magnetic testcases Coil3DEdge***
	  
      Sketch of x-z-crosssection:
      
      z-axis
      |
      | air
      |________ _
	  |        | | 
 	  |  core  |coil 
  	  |        | | 
 	  |        | | 
	  |        | | 
	  |________|_|_________ x-axis
      
	  Idea behind testcase:
	  - application of hysteresis models to 3D geometry
	  - test hysteresis for magneticEdge formulation
	
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5 fileName="CylindricCoil.h5"/>
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="3d" printGridInfo="yes">
    <variableList>
      <var name="depth" value="50e-3*2"/>
      <var name="radius" value="5e-3"/>
    </variableList>
    
    <!-- now we have both jumps in conductivity and permeability 
	this case is known to be problematic for nodal elements -->
    <regionList>
      <region name="air"  material="Air"/>
      <region name="coil" material="Copper"/>
      <region name="core" material="FECO_Mayergoyz_9"/>
    </regionList>
    
    <!-- Cylindric coordinate system for coil current -->
    <coordSysList>
      <cylindric id="mid"> 
        <origin x="0" y="0" z="0"/>
        <zAxis z="1" />
        <rAxis x="1" />
      </cylindric>
    </coordSysList>
  </domain>
  
  <fePolynomialList>
    <Lagrange>
      <isoOrder>1</isoOrder>
    </Lagrange>
  </fePolynomialList>
  
  <!-- ================================= -->
  <!--  E D G E   F O R M U L A T I O N  -->
  <!-- ================================= -->
  <sequenceStep>
    <analysis>
      <transient>
        <numSteps>3</numSteps>
        <deltaT>3e-3</deltaT>
      </transient>
    </analysis>
    <pdeList>
      <magneticEdge systemId="default">
        <regionList>
          <region name="air"/>
          <region name="coil"/>
          <region name="core" nonLinIds="p"/>
        </regionList>

		<nonLinList>
			<hysteresis id="p"/>
        </nonLinList>

		<bcsAndLoads>
          <fluxParallel name="x-inner"/>
          <fluxParallel name="x-outer"/>
          <fluxParallel name="y-inner"/>
          <fluxParallel name="z-outer"/>
        </bcsAndLoads>
        
        <coilList>
          <coil id="myCoil">
		  <!-- Current unrealisticly high but basically just set to drive material into saturation -->
            <source type="current" value="4500*sample1D('inputCurrent2.txt',t,1)"/>
              <part>
                <regionList>
                  <region name="coil"/>
                </regionList>
                <direction>
                  <analytic coordSysId="mid">
                    <comp dof="phi" value="1"/>
                  </analytic>
                </direction>
                <wireCrossSection area="1e-6"/>
                <resistance value="0"/> 
              </part>
            </coil>
          </coilList>
        
        <storeResults>
          <elemResult type="magPotential">
            <allRegions/>
          </elemResult>

          <elemResult type="magFluxDensity">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
		 
		        <elemResult type="magFieldIntensity">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
		        </elemResult>
          
          <elemResult type="magPolarization">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
          
		  <elemResult type="magEddyCurrentDensity">
			  <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
		   </elemResult>
	  
          <regionResult type="magEnergy">
            <allRegions outputIds="txt"/>
          </regionResult>
         
        </storeResults>
        
      </magneticEdge>
    </pdeList>
	
	<linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <!-- setup solution process for non-linear, hysteretic system -->
            <!-- minLoggingToTerminal = 0 > disable direct output of non-linear iterations;
              = 1 > write incremental, residual error, linesearch factor etc. to cout
              after each iteration	
              = 2 > as 1 but write out overview over all iterations at end of a timestep -->
            <hysteretic loggingToFile="yes" loggingToTerminal="0">
              <solutionMethod>
		<!-- Global fixpoint method based on H-iteration (i.e., incomplete inversion on element level
			many tiny iterations steps on system level; quite fast and reliable; contraction factor
			should be larger than 1 to ensure contraction; the higher the value, however, the slower the
			overall convergence process; Caution: if hyst operator is traced insufficiently or if the
			the slope of the B-H curve is not estimated correctly, the resulting fixpoint-system matrix
			will not be contractive and thus the iteration will fail -->
                <Fixpoint_Global_H ContractionFactor="1.1E0"/>
              </solutionMethod>
              <lineSearch>
		<!-- global fixpoint methods usually should not require a line-search as they are already globally
			convergent; however, in some cases (see comment above) the estimate for the slopes is not
			good enough and thus the FP iteration is either not contractive or too restrictive (i.e. too slow);
			an exact linesearch like GoldenSection or QuadraticPolynomial might help here as it allows for
			overrelaxation, too; -->
                <None></None>
              </lineSearch>
              <stoppingCriteria>
		<!-- here we can define different relative and absolute stopping criteria (use oxygen and ctrl+space to
			get suggestions; the non-linear solution process is seen as successful if ALL listed criteria
			are satisfied -->
                <residual_relative value="1.0E-5" checkingFrequency="1" firstCheck="1" scaleToSystemSize="no"/>
              </stoppingCriteria>
              <evaluationDepth>
		<!-- the name already reports the function; otherwise see tooltips in oxygen for further details -->
                <evaluateAtIntegrationPoints_OneOperatorPerElement></evaluateAtIntegrationPoints_OneOperatorPerElement>
              </evaluationDepth>
              <failbackCriteria>
		<!-- additional criteria that can be used to rescue a non-linear solution process; sometimes we cannot
			get rid of the residual well enough, e.g., if we are stuck to a local minimum or if the iteration
			process is too slow; in that case it might be useful to accept some looser criteria;
			the non-linear solution process is seen to be sufficiently successful if ANY of the listed
			criteria is satisfied -->
                <residual_absolute value="1.0E-6" firstCheck="3" checkingFrequency="3" scaleToSystemSize="yes"/>
              </failbackCriteria>
		<!-- H-based fixpoint does not invert the hyst operator but basically does just one incomplete inversion
			step on element level; therefore the global convergence strongly suffers which results in many
			outer iterations -->
              <maxIter>150</maxIter>
            </hysteretic>
          </standard>
        </solutionStrategy>
      </system>
    </linearSystems>
	
   </sequenceStep>
   
  </cfsSimulation>
