<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>3D max E (Young's modulus) with is-othotropic constraints</title>
    <authors>
      <author>Fabian Wein</author>
    </authors>
    <date>2015-11-27</date>
    <keywords>
      <keyword>SIMP</keyword>
    </keywords>
    <references>none</references>
    <isVerified>no</isVerified>
    <description>Coarse truncated 3D max E problem. The initial guess looks already similar but is not iso-orthotropic.
    The last design is not symetric yet. Due to the high penalization make sure the initial design has not too small
    values, otherwise cholmod complains.
    </description>
  </documentation>

  <fileFormats>
    <output>
      <hdf5/>
      <info/>
    </output>
    <materialData file="mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="3d">
    <regionList>
      <region name="mech" material="99lines"/>
    </regionList>
    <nodeList>
      <nodes name="center">
        <coord x="0.5" y="0.5" z="0.5"/>
      </nodes>
    </nodeList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <mechanic subType="3d">
        <regionList>
          <region name="mech" />
        </regionList>

        <bcsAndLoads>
          <periodic secondary="left"    primary="right" dof="x" quantity="mechDisplacement"/> 
          <periodic secondary="left"    primary="right" dof="y" quantity="mechDisplacement"/>
          <periodic secondary="left"    primary="right" dof="z" quantity="mechDisplacement"/>           
          <periodic primary="bottom" secondary="top" dof="x"  quantity="mechDisplacement" /> 
          <periodic primary="bottom" secondary="top" dof="y"  quantity="mechDisplacement" /> 
          <periodic primary="bottom" secondary="top" dof="z"  quantity="mechDisplacement" />            
          <periodic primary="front"  secondary="back" dof="x"  quantity="mechDisplacement" /> 
          <periodic primary="front"  secondary="back" dof="y"  quantity="mechDisplacement" /> 
          <periodic primary="front"  secondary="back" dof="z"  quantity="mechDisplacement" />

          <fix name="center">
            <comp dof="x"/>
            <comp dof="y"/>
            <comp dof="z"/> 
          </fix>            
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions />
          </nodeResult>
          <elemResult type="mechPseudoDensity">
            <allRegions/>
          </elemResult>          
          <elemResult type="physicalPseudoDensity">
            <allRegions/>
          </elemResult>             
       </storeResults>
      </mechanic>
    </pdeList>
  </sequenceStep>

  <loadErsatzMaterial file="circular_3d-v_0.5_8.density.xml" set="last"/>

  <optimization>
    <costFunction type="youngsModulus" task="maximize" multiple_excitation="true">
      <multipleExcitation type="homogenizationTestStrains" />
    </costFunction>

    <constraint type="iso-orthotropy" mode="constraint" />

    <constraint type="volume" mode="constraint" value="0.5" bound="upperBound" />
    <constraint type="volume" mode="observation" access="physical" />
    <constraint type="greyness" mode="observation" access="physical" />
    
    <optimizer type="scpip" maxIterations="5">
      <snopt>
        <option name="verify_level" type="integer" value="0"/>
      </snopt>
    </optimizer>

    <ersatzMaterial method="simp" material="mechanic" region="mech">
      <filters>
        <filter neighborhood="maxEdge" value="1.3" type="density"/>
      </filters>
    
      <design name="density" initial="0.5" upper="1" region="mech" physical_lower="1e-9" />

      <transferFunction type="simp" application="mech" design="density" param="5" />
      <export write="iteration"/>
    </ersatzMaterial>
    <commit stride="1" mode="forward"/>
  </optimization>  

</cfsSimulation>
