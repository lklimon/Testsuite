<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
               xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd"
               xmlns="http://www.cfs++.org/simulation">
  
  <documentation>
    <title>transient channel coupling (LinFlow to acouPotential)</title>
    <authors>
      <author>dmayrhof</author>
    </authors>
    <date>2021-11-22</date>
    <keywords>
      <keyword>flow</keyword>
      <keyword>acoustic</keyword>
      <keyword>output</keyword>
    </keywords>
    <references> 
          Non Just functionality test
    </references>
    <isVerified>yes</isVerified>
    <description>
      Tranient simulation of the coupling between the LinFlow-PDE and the acoustic-PDE in the acouPotential formulation for a 1D-channel. We excite with a velocity and check the result by comparing it to the 1D solution (p_a = v_a*rho_0*c_0)
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <!--<hdf5 fileName="PlaneWave2DacouPot.h5ref" readEntities="Channel_LF;Channel_Acou;Excite;LF_Bot;LF_Top;LF_IF;Acou_IF;ABC"/>--> <!-- readEntities works, but the testcases fail for whatever reason -->
      <cdb fileName="Channel_mesh.cdb"/>  
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <regionList>
      <region name="Channel_LF" material="FluidMat"/>
      <region name="Channel_Acou" material="FluidMat"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="Excite"/>
      <surfRegion name="LF_Bot"/>
      <surfRegion name="LF_Top"/>
      <surfRegion name="LF_IF"/>
      <surfRegion name="Acou_IF"/>
      <surfRegion name="ABC"/>
    </surfRegionList> 
    
    <ncInterfaceList>
      <ncInterface name="NCI" primarySide="LF_IF" secondarySide="Acou_IF"/>
    </ncInterfaceList>
    
    <nodeList>
      <nodes name="S1">
        <coord x="400e-3" y="0" z="0"/>
      </nodes>
    </nodeList>
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="orderVel">
      <isoOrder>2</isoOrder> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="orderPres">
       <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>

  <integrationSchemeList>
    <scheme id="integVel">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="integPres">
      <method>Gauss</method>
      <order>4</order>
      <mode>absolute</mode>
    </scheme>    
  </integrationSchemeList>  
  
  <sequenceStep>
    <analysis>
      <transient>
        <numSteps>50</numSteps>
        <deltaT>5e-5</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <fluidMechLin formulation="compressible" presPolyId="orderPres" velPolyId="orderVel">
        <regionList>
          <region name="Channel_LF"/>
        </regionList>
               
        <bcsAndLoads> 
          <noSlip name="LF_Top">
            <comp dof="y"/>         
          </noSlip>
          <noSlip name="LF_Bot">
            <comp dof="y"/>         
          </noSlip>
          <velocity name="Excite">
            <comp dof="x" value="sin(2*pi*1000*t)*(1-exp(-t/(3e-3)))"/>
          </velocity>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions outputIds="h5"/>         
          </nodeResult>
        </storeResults>
      </fluidMechLin>
      
      <acoustic formulation="acouPotential">
        <regionList>
          <region name="Channel_Acou" polyId="orderPres"/>
        </regionList>

        <storeResults>
          <nodeResult type="acouPotential">
            <allRegions outputIds="h5"/>
          </nodeResult>
          <nodeResult type="acouPotentialD1">
            <allRegions outputIds="h5"/>
            <nodeList>
              <nodes name="S1" outputIds="txt"/>
            </nodeList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
    
    <couplingList>
      <direct>
        <linFlowAcouDirect>
          <ncInterfaceList>
            <ncInterface name="NCI"/>
          </ncInterfaceList>
        </linFlowAcouDirect>
      </direct>
    </couplingList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <matrix reordering="noReordering"/>
          </standard>
        </solutionStrategy>
        <solverList>
          <pardiso/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
