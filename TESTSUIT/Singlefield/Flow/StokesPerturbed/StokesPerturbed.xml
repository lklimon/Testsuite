<?xml version="1.0"?>

<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd"
 xmlns="http://www.cfs++.org/simulation">
  
  <documentation>
    <title>Steady channel flow - Stokes problem</title>
    <authors>
      <author>Simon Triebenbacher</author>
    </authors>
    <date>2012-03-09</date>
    <keywords>
      <keyword>CFD</keyword>
      <keyword>FluidMechPerturbedPDE</keyword>
    </keywords>    
    <references>
      @PHDTHESIS{Link2008,
      author = {Gerhard Link},
      title = {A Finite Element Scheme for Fluid--Solid--Acoustics Interactions
      and its Application to Human Phonation},
      school = {University Erlangen-Nuremberg},
      year = {2008},
      month = dec,
      file = {:GerhardLinkDissertation.pdf:PDF},
      owner = {simon},
      timestamp = {2009.04.24},
      url = {http://www.opus.ub.uni-erlangen.de/opus/volltexte/2008/1203/}
      }
    </references>
    <isVerified>yes</isVerified>
    <description>cf. theory.pdf</description>
  </documentation>
  
  
  <fileFormats>
    <input>
      <!--<gmsh fileName="StokesPerturbed.msh"/>-->
      <hdf5 fileName="StokesPerturbed.h5ref"/>
    </input>
    <output>
      <hdf5 id="h5"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane" printGridInfo="yes">
    <variableList>
      <var name="H"    value="1"/>
      <var name="L"    value="2"/>
    </variableList>
    <regionList>
      <region name="channel" material="FluidMat"/>
    </regionList>
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="velPolyId">
       <gridOrder/> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="presPolyId">
       <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>

  <integrationSchemeList>
    <scheme id="velIntegId">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="presIntegId">
      <method>Gauss</method>
      <order>4</order>
      <mode>absolute</mode>
    </scheme>    
  </integrationSchemeList>  
  
  <sequenceStep>
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <fluidMech systemId="fluid" formulation="perturbed">
        <regionList>
          <region name="channel" polyId="default"/>
        </regionList>
        
        <presSurfaceList>
          <presSurf name="bottom"/>
          <presSurf name="top"/>
          <presSurf name="left"/>
          <presSurf name="right"/>
        </presSurfaceList>
        
        <bcsAndLoads>
          <noPressure name="right"/>
          
          <noSlip name="top">
            <comp dof="x"/>
            <comp dof="y"/>
          </noSlip>
          <noSlip name="bottom">
            <comp dof="x"/>
            <comp dof="y"/>
          </noSlip>         
          <noSlip name="right">
            <comp dof="y"/>
          </noSlip>
          <velocity name="left">
            <comp dof="x" value="0.5 - 0.5 * ((y-0.5)/0.5)*((y-0.5)/0.5)"/>
            <comp dof="y" value="0"/>
          </velocity>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions/>         
          </nodeResult>
          <!--sensorArray fileName="vel-line.txt" type="fluidMechVelocity">
            <parametric>
            <list comp="x" start="1" stop="1" inc="0"/>
            <list comp="y" start="0" stop="1" inc="0.01"/>
            </parametric>
          </sensorArray-->          
        </storeResults>
      </fluidMech>
    </pdeList>
    
    <linearSystems>
      <system id="fluid">
        <solutionStrategy>
          <standard>
            <setup idbcHandling="elimination" staticCondensation="no"/>
            <exportLinSys baseName="stokes_mat_vel"/>
            <matrix storage="sparseNonSym"/>
            <solver id="default"/>
          </standard>
        </solutionStrategy>
        <solverList>
          <umfpack/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
