<?xml version="1.0"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">

  <documentation>
    <title>Voltage Driven Air-Core Coil (Edge Formulation)</title>
    <authors>
      <author>dperchto</author>
    </authors>
    <date>2014-10-01</date>
    <keywords>
      <keyword>magneticEdge</keyword>
      <keyword>coil</keyword>
      <keyword>transient</keyword>
    </keywords>
    <references>
      Kaltenbacher M., Num. Sim. of Mech. Sens. and Act., 
      2nd Edition, p. 211f.
      
      http://de.wikipedia.org/wiki/Zylinderspule
    </references>
    <isVerified>yes</isVerified>
    <description>
      This is an eighth-symmetric model of a long, thin air-core coil.
      
      The coil is driven by an ideal voltage source in series with
      a 1 Ohm resistor.
      
      In order to obtain the correct values for the linked flux,
      inductance, etc., the symmetry has to be taken into account.
      The voltage specified must be a quarter of the original voltage
      because the model contains only a quarter of the contour which
      would be considered for calculating the voltage.
      The cross-section of the wire and the series resistor have to be
      half of the original. The resistance is halved because the path
      is quartered but the cross-section is halved.
      Concerning the results of the simulation, the current has to be
      doubled, the linked flux has to be multiplied by 4 and the energy
      must be multiplied by 8.
      
      length of coil       ..... l = 10 cm
      inner radius of coil ..... r_1 = 1 cm
      number of turns      ..... N = 50
      series resistor      ..... R = 1 V/A
      
      magnetic resistance  ..... R_m = l/(mu_0*r_1^2*pi) = 2.533e8 A/(V*s)
      inductance           ..... L = N^2/R_m = 9.87e-6 H (about 10 µH)
      
      This results in a time constant of about 10 µs for the RL-series.
      The time constant is useful to check the corrections due to
      the symmetry. For quarter- and half-symmetric setups, i.e. 2 and 1
      planes of symmetry, the corrections are different depending on
      which planes are used.
      
      Since this example is linear and there is no permanent magnet, the
      inductance can be calculated in different, equivalent ways
      L = Psi/i = (dPsi)/(di) = 2*W/i^2 = (d^2W)/(di^2) = (d^2W_co)/(di^2),
      where W is the magnetic energy, W_co the magnetic co-energy and Psi
      the linked flux.
      The energies can be calculated as
      W = int(Psi di), W_co = int(i dPsi)
      and are equal for linear material parameters and no permanent magnet.
    </description>
  </documentation>

  <fileFormats>
    <input>
      <hdf5 fileName="aircoil3Deighth.h5"/>
    </input>
    <output>
      <hdf5 id="hdf5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="3d">
    <regionList>
      <region name="air" material="air"/>
      <region name="coil" material="air"/>
    </regionList>
    <surfRegionList>
      <surfRegion name="BnZero"/>
      <surfRegion name="mainFlux"/>
    </surfRegionList>
    <coordSysList>
      <cylindric id="coilCyl">
        <origin x="0.0" y="0.0" z="0.0"/>
        <zAxis x="0.0" y="0.0" z="1.0"/>
        <rAxis x="1.0" y="0.0" z="0.0"/>
      </cylindric>
    </coordSysList>
  </domain>

  <fePolynomialList>
    <Legendre id="default">
      <isoOrder>0</isoOrder>
    </Legendre>
  </fePolynomialList>

  <sequenceStep>
    <analysis>
      <transient>
        <numSteps>25</numSteps>
        <deltaT>2e-6</deltaT>
      </transient>
    </analysis>

    <pdeList>
      <magneticEdge>
        <regionList>
          <region name="air"/>
          <region name="coil"/>
        </regionList>

        <bcsAndLoads>
          <fluxParallel name="BnZero"/>
        </bcsAndLoads>

        <coilList>
          <coil id="AirCoreCoil">
            <source type="voltage" value="0.25"/><!-- quarter of original -->
            <part id="1">
              <regionList>
                <region name="coil"/>
              </regionList>
              <direction>
                <analytic coordSysId="coilCyl">
                  <comp dof="phi" value="1.0"/>
                </analytic>
              </direction>
              <wireCrossSection area="2.5e-6"/><!-- half of original -->
              <resistance value="0.5"/><!-- half of original because quarter of contour, half of cross-section -->
            </part>
          </coil>
        </coilList>

        <storeResults>
          <elemResult type="magFluxDensity">
            <allRegions outputIds="hdf5"/>
          </elemResult>
          <coilResult type="coilCurrent">
            <coilList>
              <coil id="AirCoreCoil" outputIds="txt"/>
            </coilList>
          </coilResult>
          <coilResult type="coilLinkedFlux">
            <coilList>
              <coil id="AirCoreCoil" outputIds="txt"/>
            </coilList>
          </coilResult>
          <surfRegionResult type="magFlux">
            <surfRegionList>
              <surfRegion name="mainFlux" outputIds="txt"/>
            </surfRegionList>
          </surfRegionResult>
          <regionResult type="magEnergy">
            <allRegions outputIds="txt"/>
          </regionResult>
        </storeResults>
      </magneticEdge>
    </pdeList>

    <linearSystems>
      <system>
        <solverList>
          <directLU/>
        </solverList>
      </system>
    </linearSystems>

    
  </sequenceStep>
</cfsSimulation>
