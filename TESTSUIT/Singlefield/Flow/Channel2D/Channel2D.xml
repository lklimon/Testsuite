<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
  
  <documentation>
    <title>Channel flow - Navier-Stokes problem</title>
    <authors>
      <author>Manfred Kaltenbacher</author>
    </authors>
    <date>2013-12-31</date>
    <keywords>
      <keyword>CFD</keyword>
    </keywords>    
    <references>
      @PHDTHESIS{Link2008,
      author = {Gerhard Link},
      title = {A Finite Element Scheme for Fluid--Solid--Acoustics Interactions
      and its Application to Human Phonation},
      school = {University Erlangen-Nuremberg},
      year = {2008},
      month = dec,
      file = {:GerhardLinkDissertation.pdf:PDF},
      owner = {simon},
      timestamp = {2009.04.24},
      url = {http://www.opus.ub.uni-erlangen.de/opus/volltexte/2008/1203/}
      }
    </references>
    <isVerified>yes</isVerified>
    <description> 
       Simple channel flow driven by an input velocity distribution  
    </description>
  </documentation>
  
  
  <fileFormats>
    <input>
      <gmsh fileName="stokes1.msh"/>
    </input>
    <output>
      <hdf5 id="h5"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane" printGridInfo="yes">
    <regionList>
      <region name="channel" material="FluidMat2"/>
    </regionList>
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="velPolyId">
       <gridOrder/> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="presPolyId">
       <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>

  <integrationSchemeList>
    <scheme id="velIntegId">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="presIntegId">
      <method>Gauss</method>
      <order>4</order>
      <mode>absolute</mode>
    </scheme>    
  </integrationSchemeList>  
  
  <sequenceStep>
    <analysis>
      <transient>
        <numSteps>5</numSteps>
        <deltaT>0.1</deltaT>
      </transient>
    </analysis>

    
    <pdeList>
      <fluidMech systemId="fluid">
        <regionList>
          <region name="channel" polyId="default"/>
        </regionList>
        
        <presSurfaceList>
          <presSurf name="bottom"/>
          <presSurf name="top"/>
          <presSurf name="left"/>
          <presSurf name="right"/>
        </presSurfaceList>
        
        <bcsAndLoads>
          <noPressure name="right"/>
          
          <noSlip name="top">
            <comp dof="x"/>
            <comp dof="y"/>
          </noSlip>
          <noSlip name="bottom">
            <comp dof="x"/>
            <comp dof="y"/>
          </noSlip>         
          <noSlip name="right">
            <comp dof="y"/>
          </noSlip>
          <velocity name="left">
            <comp dof="x" value="(1-(((y-0.5)^2)/0.5^2))"/>
            <comp dof="y" value="0"/>
          </velocity>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions/>         
          </nodeResult>
        </storeResults>
      </fluidMech>
    </pdeList>
    
    <linearSystems>
      <system id="fluid">
        <solutionStrategy>
          <standard>
            <matrix storage="sparseNonSym"/>
            <nonLinear logging="yes" method="fixPoint">
                   <lineSearch/>
                   <incStopCrit> 1e-4</incStopCrit>
                   <resStopCrit> 1e-4</resStopCrit>
                   <maxNumIters> 20  </maxNumIters>
            </nonLinear>
          </standard>
        </solutionStrategy>
        <solverList>
          <pardiso/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
