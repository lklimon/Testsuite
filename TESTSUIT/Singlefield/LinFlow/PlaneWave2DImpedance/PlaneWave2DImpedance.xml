<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
               xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd"
               xmlns="http://www.cfs++.org/simulation">
  
  <documentation>
    <title>Transient LinFlow-PDE without viscous effects to test impedance calculations</title>
    <authors>
      <author>dmayrhof</author>
    </authors>
    <date>2021-11-25</date>
    <keywords>
      <keyword>flow</keyword>
      <keyword>output</keyword>
    </keywords>
    <references> 
      Non Just functionality test
    </references>
    <isVerified>yes</isVerified>
    <description>
      1D problem (plane wave in a channel) with pressure excitation.
      Since no viscous effects are included, the setup can be seen as a 1D acoustic problem.
      We evaluate the impedance at two interfaces - one at a certain distance where we can evaluate 
      the impedance using the retarded time and one at a second distance and at an angle where 
      the proejction will lead to different values for the impedance.
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5 fileName="PlaneWave2DImpedance.h5ref"/>
      <!--<cdb fileName="Channel_mesh.cdb"/>-->
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <regionList>
      <region name="Channel_L" material="FluidMat"/>
      <region name="Channel_M" material="FluidMat"/>
      <region name="Channel_R" material="FluidMat"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="Excite"/>
      <surfRegion name="BC_Bot"/>
      <surfRegion name="BC_Top"/>
      <surfRegion name="BC_Right"/>
      <surfRegion name="IF_L"/>
      <surfRegion name="IF_R"/>
    </surfRegionList> 
    
    <nodeList>
      <nodes name="S1">
        <coord x="0" y="0" z="0"/>
      </nodes>
      <nodes name="S2">
        <coord x="20e-3" y="0" z="0"/>
      </nodes>
    </nodeList>
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="orderVel">
      <isoOrder>2</isoOrder> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="orderPres">
       <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>

  <integrationSchemeList>
    <scheme id="integVel">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="integPres">
      <method>Gauss</method>
      <order>4</order>
      <mode>absolute</mode>
    </scheme>    
  </integrationSchemeList>  
  
  <sequenceStep>
    <analysis>
      <transient>
        <numSteps>12</numSteps>
        <deltaT>5e-5</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <fluidMechLin formulation="compressible" presPolyId="orderPres" velPolyId="orderVel">
        <regionList>
          <region name="Channel_L"/>
          <region name="Channel_M"/>
          <region name="Channel_R"/>
        </regionList>
        
        <bcsAndLoads> 
          <noSlip name="BC_Top">
            <comp dof="y"/>         
          </noSlip>
          <noSlip name="BC_Bot">
            <comp dof="y"/>         
          </noSlip>
          <noSlip name="BC_Right">
            <comp dof="x"/>
            <comp dof="y"/>         
          </noSlip>
          <pressure name="Excite" value="sin(2*pi*1000*t)*(1-exp(-t/(3e-3)))"/>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions outputIds="h5"/>   
            <nodeList>
              <nodes name="S1" outputIds="txt"/>
              <nodes name="S2" outputIds="txt"/>
            </nodeList>       
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions outputIds="h5"/> 
            <nodeList>
              <nodes name="S1" outputIds="txt"/>
              <nodes name="S2" outputIds="txt"/>
            </nodeList>        
          </nodeResult>
          <surfElemResult type="fluidMechNormalVelocity">
            <surfRegionList>
              <surfRegion name="IF_L" outputIds="txt,h5"/>
              <surfRegion name="IF_R" outputIds="txt,h5"/>
            </surfRegionList>
          </surfElemResult>
          <surfElemResult type="fluidMechSurfaceImpedance">
            <surfRegionList>
              <surfRegion name="IF_L" outputIds="txt,h5"/>
              <surfRegion name="IF_R" outputIds="txt,h5"/>
            </surfRegionList>
          </surfElemResult>
          <surfRegionResult type="fluidMechImpedance">
            <surfRegionList>
              <surfRegion name="IF_L" outputIds="txt,h5"/>
              <surfRegion name="IF_R" outputIds="txt,h5"/>
            </surfRegionList>
          </surfRegionResult>
        </storeResults>
      </fluidMechLin>
    </pdeList>

    <linearSystems>
      <system>
        <solverList>
          <directLU/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
