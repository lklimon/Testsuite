<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
    <documentation>
        <title>buckling 3D euler_beam</title>
        <authors>
            <author>Antonio Prvulovic</author>
            <author>Matthias Geist</author>
        </authors>
        <date>2019-12-13</date>
        <keywords>
            <keyword>buckling</keyword>
        </keywords>
        <references></references>
        <isVerified>no</isVerified>
        <description>
            This is a 3D model of a Euler beam, clamped on one sides, free on the other. 
            We apply a reference force in step_1 and calculate an proportionality factor in step_2.
            Analytical solution:
            dimension l/b/h  =  100/4/3
            l_c = l * 2 = 200
            
            I_z = h^3 * b / 12 = 9
            P_z_critical = pi^2 * E * I_z / l_c^2 = 222.07
            
            I_y = 16
            P_y_critical = 394.78
        </description>
    </documentation>
    <fileFormats>
        <input>
            <cdb fileName="mesh_quad.cdb" />
        </input>
        <output>
            <hdf5/>
        </output>
        <materialData file="./mat.xml" format="xml"/>
    </fileFormats>
    <domain geometryType="3d">
        <variableList>
            <var name="E" value="100e03"/>
            <var name="nu" value="0.3"/>
            <var name="loadTotal" value="1"/>
            <!-- 
      pressure Loads are defines as reversed normal traction forces
       -->
            <var name="area" value="3*4"/>
            <var name="pressure_Load" value="loadTotal/area"/>
        </variableList>
        <regionList> 
            <region name="beam" material="elastic"/> 
        </regionList>
    </domain>
    
    <sequenceStep index="1">
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <mechanic subType="3d" >
                <regionList>
                    <region name="beam"/>
                </regionList>        
                <bcsAndLoads>
                    <fix name="BC_fix">
                        <comp dof="x"/>
                        <comp dof="y"/>
                        <comp dof="z"/>
                    </fix>
                    <pressure value="pressure_Load" name="reference_Load"/>
                </bcsAndLoads>
                <storeResults>
                    <elemResult type="mechStress">
                        <allRegions/>
                    </elemResult>
                    <nodeResult type="mechDisplacement" >
                        <allRegions/>
                    </nodeResult>
                </storeResults>
            </mechanic>
        </pdeList>
    </sequenceStep>
    
    <sequenceStep index="2">
        <analysis>
            <buckling>
                
                <maxVal>6000</maxVal>
                <minVal>0</minVal>
                
                <calcModes normalization="max" />
            </buckling>
        </analysis>
        <pdeList>
            <mechanic subType="3d" >
                <regionList>
                    <region name="beam"/>
                </regionList>        
                <bcsAndLoads>
                    <fix name="BC_fix">
                        <comp dof="x"/>
                        <comp dof="y"/>
                        <comp dof="z"/>
                    </fix>
                    <referenceLoad region="beam">
                        <referenceStress sequenceStep="1"/>
                    </referenceLoad>
                    <!--referenceLoad region="beam"-->
                    <!-- if linear buckling problem is required add displacement, -->
                    <!--otherwise the conventional buckling problem is solved -->
                    <!-- referenceDisplacement sequenceStep="1"/-->
                    <!--/referenceLoad-->
                </bcsAndLoads>
                <storeResults>
                    <nodeResult type="mechDisplacement" >
                        <allRegions/>
                    </nodeResult>
                </storeResults>
            </mechanic>
        </pdeList>
        <linearSystems>
            <system>
                <solutionStrategy>
                    <standard>
                        <!-- 
            <exportLinSys differential_stiffness="true" stiffness="true"/>
            -->
                        <matrix reordering="default" storage="sparseNonSym"/>
                        <eigenSolver id="feast"/>
                    </standard>
                </solutionStrategy>
                <eigenSolverList>
                    <phist id="phist"/>
                    <arpack id="arpack"/>
                    <feast id="feast">
                        <stopCrit>6</stopCrit><!-- decrease accuracy -->
                        <m0>10</m0><!-- estimate for number of eigenvalues, makes FEAST faster -->
                    </feast>
                </eigenSolverList>
            </system>
        </linearSystems>
    </sequenceStep>
    
</cfsSimulation>