<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
               xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd"
               xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>Surf2VolMapping3D</title>
    <authors>
      <author>pheidegger</author>
    </authors>
    <date>2023-09-11</date>
    <keywords>
      <keyword>acoustic</keyword>
      <keyword>harmonic</keyword>
    </keywords>
    <references> 
      Non Just functionality test
    </references>
    <isVerified>yes</isVerified>
    <description>
      This test aims to cover the correct funciton of the integration point mapping between volume and surface elements in 2D.
      It contains Quad and Tri elements and applies a normalVelocity BC on all edges. The edges are defined in 2 different ways to 
      cover all possible cases. The correct mapping is tested indirectly, in an assert for the debug build.
      In the hope to being able to test also in a non-debug build, a gradient field is applied on the BCs.
    </description>
  </documentation>
  <!--***************************    INPUTs and OUTPUTs    ***********************-->
  <fileFormats> 
    <input>
      <hdf5 fileName="./Surf2VolMapping2D.h5ref"/>
    </input>
    <output>
      <hdf5 id="hdf5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  <!--***************************  REGIONSs, SURFACEs and NODEs  ***********************-->
  <domain geometryType="plane">
    <variableList>
      <var name="c0" value="1"/>
      <var name="p0" value="1"/>
      <var name="rho" value="1"/>
      <var name="f0" value="1"/>
    </variableList>
    <regionList> 
      <region name="S_Quad" material="fakefluid"/>
      <region name="S_Tri" material="fakefluid"/>
    </regionList>
    <surfRegionList>
      <surfRegion name="L_Quad11"/>
      <surfRegion name="L_Quad12"/>
      <surfRegion name="L_Quad21"/>
      <surfRegion name="L_Quad22"/>
      <surfRegion name="L_Quad31"/>
      <surfRegion name="L_Quad32"/>
      <surfRegion name="L_Quad41"/>
      <surfRegion name="L_Quad42"/>
      <surfRegion name="L_Tri11"/>
      <surfRegion name="L_Tri12"/>
      <surfRegion name="L_Tri21"/>
      <surfRegion name="L_Tri22"/>
      <surfRegion name="L_Tri31"/>
      <surfRegion name="L_Tri32"/>
    </surfRegionList>
  </domain>
  <!--***************************  ANALYSIS  ***********************-->
  <sequenceStep index="1">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>    
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="S_Quad"/>
          <region name="S_Tri"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="L_Quad11" value="p0*(x+y+z)"/>
          <normalVelocity name="L_Quad21" value="p0*(x+y+z)"/>
          <normalVelocity name="L_Tri11" value="p0*(x+y+z)"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="S_Quad"/>
              <region name="S_Tri"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>

  <sequenceStep index="2">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>    
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="S_Quad"/>
          <region name="S_Tri"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="L_Quad12" value="p0*(x+y+z)"/>
          <normalVelocity name="L_Quad22" value="p0*(x+y+z)"/>
          <normalVelocity name="L_Tri12" value="p0*(x+y+z)"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="S_Quad"/>
              <region name="S_Tri"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>

  <sequenceStep index="3">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="S_Quad"/>
          <region name="S_Tri"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="L_Quad31" value="p0*(x+y+z)"/>
          <normalVelocity name="L_Quad41" value="p0*(x+y+z)"/>
          <normalVelocity name="L_Tri21" value="p0*(x+y+z)"/>
          </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="S_Quad"/>
              <region name="S_Tri"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>

  <sequenceStep index="4">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="S_Quad"/>
          <region name="S_Tri"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="L_Quad32" value="p0*(x+y+z)"/>
          <normalVelocity name="L_Quad42" value="p0*(x+y+z)"/>
          <normalVelocity name="L_Tri22" value="p0*(x+y+z)"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="S_Quad"/>
              <region name="S_Tri"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>

  <sequenceStep index="5">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="S_Quad"/>
          <region name="S_Tri"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="L_Tri31" value="p0*(x+y+z)"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="S_Quad"/>
              <region name="S_Tri"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>

  <sequenceStep index="6">
    <analysis>
      <harmonic>
        <frequencyList>
          <freq value="f0"/>
        </frequencyList>
      </harmonic>
    </analysis>
    <!--***************************  PDE LISTs  ***********************-->
    <pdeList>
      <acoustic formulation="acouPressure">
        <regionList>
          <region name="S_Quad"/>
          <region name="S_Tri"/>
        </regionList>
        <!-- ************************************************************ BCS / SOURCES ****************************************************** -->
        <bcsAndLoads>
          <normalVelocity name="L_Tri32" value="p0*(x+y+z)"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPressure">
            <regionList>
              <region name="S_Quad"/>
              <region name="S_Tri"/>
            </regionList>
          </nodeResult>
        </storeResults>
      </acoustic>
    </pdeList>
  </sequenceStep>
</cfsSimulation>
