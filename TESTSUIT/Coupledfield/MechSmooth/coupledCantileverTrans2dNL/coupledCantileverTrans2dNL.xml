<cfsSimulation xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
https://opencfs.gitlab.io/cfs/xml/CFS-Simulation/CFS.xsd">
 
  <documentation>
    <title>coupledCantileverTrans2dNL</title>
    <authors>
      <author>Dominik Mayrhofer</author>
    </authors>
    <date>2022-11-16</date>
    <keywords>
      <keyword>mechanic</keyword>
      <keyword>smooth</keyword>
    </keywords>
    <references> 
      Non Just functionality test
    </references>
    <isVerified>yes</isVerified>
    <description>
   	  This test is similar to the testcase cantileverTrans2dNL for mechanics where a cantilever is excited with a force. The deformation itself is large, hence, a nonlinear solution strategy is applied. The only difference is that in this test an air domain is iterativel coupled where the smoothPDE is solved. The setup consists of a simple forward coupling (mechanics->smooth) and should test the functionality of the sub-iterations from the nonlinear problem inside the iterations stemming from the coupling.
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5 fileName="coupledCantileverTrans2dNL.h5ref"/>
      <!--<cdb fileName="cantilever.cdb"/>-->
    </input>
    <output>
      <hdf5 id="hdf"/>
    </output>
    <materialData/>
  </fileFormats>
  
  <domain geometryType="plane">
    <regionList>
      <region material="silizium" name="cantilev"/>
      <region material="FluidMat" name="air"/>
    </regionList>
  </domain>

  <sequenceStep index="1">
  
    <analysis>
      <transient>
        <numSteps>20</numSteps>
        <deltaT>5e-05</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <mechanic subType="planeStrain">

        <regionList>
          <region name="cantilev" nonLinIds="_id_geometric_1" dampingId="_id_rayleigh_1"/>
        </regionList>

        <nonLinList>
          <geometric id="_id_geometric_1"/>
        </nonLinList>

        <dampingList>
          <rayleigh id="_id_rayleigh_1"/>
        </dampingList>

        <bcsAndLoads>
          <fix name="BC_Fix">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <forceDensity name="BC_Load"> <!-- We can't use a force here since Trelis gives us surfaces instead of nodes and we can only apply nodal forces atm -->
            <comp dof="y" value="-40000*1e3 * sin(2*pi*1e3*t)"/> <!-- 1e3: Scaling factor to get the same result as when using force instead of forceDensity on a set of nodes -->
          </forceDensity>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="mechDisplacement" complexFormat="amplPhase">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </mechanic>

      <smooth subType="planeStrain">
        <regionList>
          <region name="air"/>
        </regionList>

        <bcsAndLoads>
          <fix name="BC_Fix_smooth">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          
          <displacement name="BC_IF">
            <coupling pdeName="mechanic">
              <quantity name="mechDisplacement" />
            </coupling>
          </displacement>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions/>
          </nodeResult>
          <nodeResult type="smoothVelocity">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </smooth>
    </pdeList>

    <couplingList>
      <iterative PDEorder="mechanic;smooth">
        <convergence logging="yes" maxNumIters="20" stopOnDivergence="no">
          <quantity name="smoothDisplacement" value="1e-3" normType="rel"/>
          <quantity name="smoothVelocity" value="1e-3" normType="rel"/>
          <quantity name="mechDisplacement" value="1e-3" normType="rel"/>
        </convergence>
        <geometryUpdate>
          <region name="cantilev"/>
          <region name="air"/>
        </geometryUpdate>
      </iterative>
    </couplingList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <nonLinear logging="yes" method="fixPoint">
              <lineSearch type="none"/>
              <incStopCrit>0.001</incStopCrit>
              <resStopCrit>0.001</resStopCrit>
              <maxNumIters>200</maxNumIters>
            </nonLinear>
          </standard>
        </solutionStrategy>
      </system>
    </linearSystems>
    
  </sequenceStep>
  
</cfsSimulation>
