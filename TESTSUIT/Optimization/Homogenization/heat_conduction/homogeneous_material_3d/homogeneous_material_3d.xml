<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation">
  
  <documentation>
    <title>Heat_conduction_homogeneous_material_3d</title>
    <authors>
      <author>Bich Ngoc Vu</author>
    </authors>
    <date>2020-02-24</date>
    <keywords>
      <keyword>SIMP</keyword>
    </keywords>
    <references>S. Torquato: Theory of Random Heterogeneous Materials</references>
    <isVerified>yes</isVerified>
    <description> Demonstrate the calculation of thermal conductivity by asymptotic homogenization of the static
    heat equation. 
    Very similar to linear elasticity, except that test strains consist only of unit vectors. Thus,
    the result is a rank-2 thermal conductivity tensor of size 2 x 2 (2D) or 3 x 3 (3D). 
    </description>
  </documentation>  
  
  <fileFormats>
    <output>
      <hdf5 directory="./" />
      <info/>
    </output>
    <materialData file="mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="3d">
    <regionList>
      <region name="mech" material="water"/>
    </regionList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <heatConduction>
        <regionList>
          <region name="mech" />
        </regionList>

        <bcsAndLoads>
           <periodic secondary="left" primary="right" quantity="heatTemperature"/>
           <periodic primary="front" secondary="back" quantity="heatTemperature"/>
           <periodic primary="top"  secondary="bottom" quantity="heatTemperature"/>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions />
          </nodeResult>
       </storeResults>
      </heatConduction>
    </pdeList>
    <linearSystems>
      <system>
        <solverList>
          <directLDL/> 
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>


  <optimization log="">
    <costFunction type="homTensor" task="minimize" multiple_excitation="true">
      <multipleExcitation type="homogenizationTestStrains" sequence="1"/>
    </costFunction>
    <optimizer type="evaluate" maxIterations="1"/>
    <ersatzMaterial method="simp" material="heat">
      <regions>
        <region name="mech"/>
      </regions>
      <design name="density" initial="1" lower="1e-3" upper="1" region="mech"/>
      <transferFunction type="identity" application="heat" design="density"/>
    </ersatzMaterial>
    <commit mode="each_forward"/>
  </optimization>  
</cfsSimulation>
